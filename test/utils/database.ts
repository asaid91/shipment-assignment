import { Container } from 'typedi';
import { env } from '../../src/env';
import * as mongoose from 'mongoose';

export const createDatabaseConnection = async (): Promise<any> => {
    // connect to mongoose
    const connection = await mongoose.connect(env.db.host);
    return connection;
};


export const closeDatabase = (connection: any) => {
    return connection.close();
};
