import { validate } from 'class-validator';
import { CreateOrder } from '../../src/api/controllers/requests/CreateOrder';



describe('OrderValidations', () => {

    test('it should always have a required validations in all inputs', async (done) => {
        const order = new CreateOrder();
        const errors = await validate(order);
        expect(errors.length).toEqual(4);
        done();
    });

    test('it should check MongoDB id format sent', async (done) => {
        const order = new CreateOrder();
        order.orderNumber = '1234';

        // MongoDB ids
        order.customerId = '1234';
        order.companyId = '1234';
        order.productId = '1234';

        const errors = await validate(order);
        expect(errors.length).toEqual(3);
        done();
    });

    test('Order validation should succeed with all required fields', async (done) => {
        const order = new CreateOrder();
        order.orderNumber = '1234';
        order.customerId = '5a85c57ff72b8a78de56c5ba';
        order.companyId = '5a85c57ff72b8a78de56c5ba';
        order.productId = '5a85c57ff72b8a78de56c5ba';
        const errors = await validate(order);
        expect(errors.length).toEqual(0);
        done();
    });

});
