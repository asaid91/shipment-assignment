import { validate } from 'class-validator';
import { EditCompany } from '../../src/api/controllers/requests/EditCompany';



describe('CompanyValidations', () => {

    test('it should always have a required validations in all inputs', async (done) => {
        const company = new EditCompany();
        const errors = await validate(company);
        expect(errors.length).toEqual(3);
        done();
    });

    test('it should check correct email format sent', async (done) => {
        const company = new EditCompany();
        company.companyName = 'test';
        company.phone = '1234';
        company.email = 'invalid_email@@';
        company.address = 'address';
        const errors = await validate(company);
        expect(errors.length).toEqual(1);
        done();
    });

    test('Company validation should succeed with all required fields', async (done) => {
        const company = new EditCompany();
        company.companyName = 'test';
        company.phone = '1234';
        company.email = 'email@gmail.com';
        company.address = 'address';
        const errors = await validate(company);
        expect(errors.length).toEqual(0);
        done();
    });

});
