import * as request from 'supertest';
import { bootstrapApp, BootstrapSettings } from '../utils/bootstrap';
import { closeDatabase } from '../../utils/database';

let settings: BootstrapSettings;

// this data should be exist on test db
const testData = {
    companyId: '5a819b39f61f2357a4c36f0e',
    company: null,
    companyName: 'company Name Test',
    phone: '34343434',
    email: 'test@test.com',
    address: 'address test',
    orders : []
};

describe('/api/company', () => {

    beforeAll(async () => settings = await bootstrapApp());
    let originalTimeout;

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    test('GET: /:id should return order info', async (done) => {
        const response = await request(settings.app)
            .get(`/api/company/${testData.companyId}`)
            .expect('Content-Type', /json/)
            .expect(200);
        expect(response.body.success).toBe(true);
        testData.company = response.body.data;
        done();
    });

    test('PUT: /:id should update company', async (done) => {
        const response = await request(settings.app)
            .put(`/api/company/${testData.companyId}`)
            .send({
                companyName: testData.companyName,
                phone: testData.phone,
                email: testData.email,
                address: testData.address
            })
            .expect('Content-Type', /json/)
            .expect(200);

        // check all data updated
        expect(response.body.data.name).toEqual(testData.companyName);
        expect(response.body.data.email).toEqual(testData.email);
        expect(response.body.data.address).toEqual(testData.address);
        expect(response.body.data.phone).toEqual(testData.phone);
        done();
    });

    test('GET: /:companyName/orders should find related orders v1', async (done) => {
        const response = await request(settings.app)
            .get(`/api/company/${testData.companyName}/orders`)
            .expect('Content-Type', /json/)
            .expect(200);
        expect(response.body.success).toBe(true);
        testData.orders = response.body.data;
        done();
    });

    test('GET: /:companyName/orders should find related orders v2', async (done) => {
        const response = await request(settings.app)
            .get(`/api/company/v2/${testData.companyId}/orders`)
            .expect('Content-Type', /json/)
            .expect(200);
        expect(response.body.success).toBe(true);
        expect(testData.orders.length).toEqual(response.body.data.length);
        done();
    });

    test('GET: /:companyName/orders should find related orders v2', async (done) => {
        const response = await request(settings.app)
            .get(`/api/company/v2/${testData.companyId}/orders`)
            .expect('Content-Type', /json/)
            .expect(200);
        expect(response.body.success).toBe(true);
        expect(testData.orders.length).toEqual(response.body.data.length);
        done();
    });

    test('GET: /:id/total should get the amount of money paid by a certain company', async (done) => {
        const response = await request(settings.app)
            .get(`/api/company/${testData.companyId}/total`)
            .expect('Content-Type', /json/)
            .expect(200);
        expect(response.body.success).toBe(true);

        // test total is correct
        let totalAmountTest = 0;
        testData.orders.map((order) => totalAmountTest += order.price);
        expect(response.body.data).toEqual(totalAmountTest);
        done();
    });

});

