import * as request from 'supertest';
import { bootstrapApp, BootstrapSettings } from '../utils/bootstrap';
import { closeDatabase } from '../../utils/database';

let settings: BootstrapSettings;
let order: any;

// this data should be exist on test db
const testData = {
    randomOrderNumber: Math.floor(100000000 + Math.random() * 900000000).toString(),
    customerId: '5a85c54cf72b8a78de56c5b7',
    companyId: '5a819b39f61f2357a4c36f0e',
    productId: '5a85c57ff72b8a78de56c5ba'
};

describe('/api/users', () => {

    beforeAll(async () => settings = await bootstrapApp());

    test('GET: / should return a list of orders', async (done) => {
        const response = await request(settings.app)
            .get('/api/orders')
            .expect('Content-Type', /json/)
            .expect(200);

        expect(response.body.success).toBe(true);
        expect(response.body.data.length).toBeGreaterThan(0);
        order = response.body.data.pop();
        done();
    });

    test('GET: /:id should return order', async (done) => {
        const response = await request(settings.app)
            .get(`/api/orders/${order.id}`)
            .expect('Content-Type', /json/)
            .expect(200);
        expect(response.body.data.id).toEqual(order.id);
        done();
    });

    test('POST: /:id should create new order', async (done) => {
        const orderRequest = {
            orderNumber: testData.randomOrderNumber,
            customerId: testData.customerId,
            companyId: testData.companyId,
            productId: testData.productId
        };
        const response = await request(settings.app)
            .post(`/api/orders`)
            .send(orderRequest)
            .expect('Content-Type', /json/)
            .expect(201);
        expect(response.body.data.orderNumber).toEqual(testData.randomOrderNumber);
        done();
    });

});
