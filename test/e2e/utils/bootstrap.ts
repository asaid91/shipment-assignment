import { mongoLoader } from './../../../src/loaders/mongoLoader';
import * as http from 'http';
import { bootstrapMicroframework } from 'microframework-w3tec';
import { Application } from 'express';
import { expressLoader } from './../../../src/loaders/expressLoader';
import { winstonLoader } from './../../../src/loaders/winstonLoader';
import { homeLoader } from './../../../src/loaders/homeLoader';
import { iocLoader } from './../../../src/loaders/iocLoader';


export interface BootstrapSettings {
    app: Application;
    server: http.Server;
    connection: any;
}

export const bootstrapApp = async (): Promise<BootstrapSettings> => {
    const framework = await bootstrapMicroframework({
        loaders: [
            winstonLoader,
            iocLoader,
            mongoLoader,
            expressLoader,
            homeLoader,
        ],
    });
    return {
        app: framework.settings.getData('express_app') as Application,
        server: framework.settings.getData('express_server') as http.Server,
        connection: framework.settings.getData('connection') as any,
    } as BootstrapSettings;
};
