import { homeLoader } from './loaders/homeLoader';
import { swaggerLoader } from './loaders/swaggerLoader';
import { expressLoader } from './loaders/expressLoader';
import { mongoLoader } from './loaders/mongoLoader';
import 'reflect-metadata';
import { banner } from './lib/banner';
import { Logger } from './lib/logger';
import { bootstrapMicroframework } from 'microframework-w3tec';
import { winstonLoader } from './loaders/winstonLoader';
import { iocLoader } from './loaders/iocLoader';
const log = new Logger(__filename);

// bootstrap all loaders are executed one by one in a sequential order.
const bootsrap = bootstrapMicroframework({
    loaders: [
        winstonLoader,
        iocLoader,
        mongoLoader,
        expressLoader,
        swaggerLoader,
        homeLoader,
    ],
});

bootsrap.then(() => banner(log));
bootsrap.catch(error => log.error('Application is crashed: ' + error, error) );
