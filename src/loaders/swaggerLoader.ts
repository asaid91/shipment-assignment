import * as path from 'path';
import * as swaggerUi from 'swagger-ui-express';
import { MicroframeworkSettings, MicroframeworkLoader } from 'microframework-w3tec';
import { env } from '../env';


export const swaggerLoader: MicroframeworkLoader = (settings: MicroframeworkSettings | undefined) => {
    if (settings && env.swagger.enabled) {
        const expressApp = settings.getData('express_app');
        const swaggerFile = require(path.join(__dirname, '..', env.swagger.file));

        // Add npm infos to the swagger doc
        swaggerFile.info = {
            title: env.app.name,
            description: env.app.description,
            version: env.app.version,
        };
        swaggerFile.host = env.app.route.replace('http://', '');
        swaggerFile.basePath = env.app.routePrefix;

        expressApp.use(
            env.swagger.route,
            swaggerUi.serve,
            swaggerUi.setup(swaggerFile)
        );

    }
};
