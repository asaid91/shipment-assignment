import { MicroframeworkSettings, MicroframeworkLoader } from 'microframework-w3tec';
import { env } from '../env';
import * as mongoose from 'mongoose';

export const mongoLoader: MicroframeworkLoader = async (settings: MicroframeworkSettings | undefined) => {

    // connect to mongoose
    const connection = await mongoose.connect(env.db.host);
    if (settings) {
        settings.setData('connection', connection);
        settings.onShutdown(() => connection.disconnect());
    }
};
