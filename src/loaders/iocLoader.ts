import { Container } from 'typedi';
import { useContainer as routingUseContainer } from 'routing-controllers';
import { MicroframeworkSettings, MicroframeworkLoader } from 'microframework-w3tec';


export const iocLoader: MicroframeworkLoader = (settings: MicroframeworkSettings | undefined) => {
    // Setup dependy injection container
    routingUseContainer(Container);

};
