import { CustomerModel } from './../models/CustomerModel';
import { BaseCustomerRepository } from './../../../base-repositories/BaseCustomerRepository';
import { Company } from '../../../../models/Company';
import { Customer } from '../../../../models/Customer';
export class CustomerRepository implements BaseCustomerRepository {
    public async findCustomer(customerId: string): Promise<Customer> {
        const product = await CustomerModel.findById(customerId);
        if (product !== null) {
            return new Customer({
                customerId: product._id.toString(),
                address: product.address
            });
        }
        return null;
    }

}
