import { Order } from './../../../../models/Order';
import { CompanyModel, ICompanyModel } from './../models/CompanyModel';
import { BaseCompanyRepository } from './../../../base-repositories/BaseCompanyRepository';
import { OrderModel } from '../models/OrderModel';
import { Company } from '../../../../models/Company';
import { Validator } from 'class-validator';
export class CompanyRepository implements BaseCompanyRepository {

    public async getTotalAmount(id: string): Promise<number> {
        const result = await OrderModel.aggregate([{
            $match: { 'company.companyId': id }
        }, {
            $group: {
                _id: null,
                // tslint:disable-next-line:object-literal-key-quotes
                total: { '$sum': '$product.price' }
            }
        },
        {
            $project: {
                _id: 0,
                total: 1
            }
        }]);
        return result.pop().total || 0;
    }
    public async getCompaniesForCertainItem(productId: string): Promise<Company[]> {
        // get list of unique companies
        const distinctCompanies = await OrderModel.distinct('company.companyId', { 'product.productId': productId });

        // get other information of those companies
        const items = await CompanyModel.find({ _id: { $in: distinctCompanies } });
        return items.map(item => {
            return new Company({
                companyId: item.id,
                name: item.name,
                address: item.address,
                phone: item.phone,
                email: item.email
            });
        });
    }

    public async editCompany(model: Company): Promise<boolean> {
        const company = await this.findById(model.companyId);
        if (company) {
            company.name = model.name;
            company.phone = model.phone;
            company.email = model.email;
            company.address = model.address;
            await company.update(company);
            // update relatives
            // TODO move it to "post" MongoDB middleware
            // we update the old field as well not to break retro compatibility,
            OrderModel.find({ 'company.companyId': model.companyId }, (err, orders) => {
                orders.forEach(order => {
                    order.companyName = model.name;
                    order.company.name = model.name;
                    order.save();
                });
            });
            return true;
        }
        return false;
    }


    public async findCompany(id: string): Promise<Company> {
        if (new Validator().isMongoId(id)) {
            const company = await this.findById(id);
            if (company !== null) {
                return new Company({
                    name: company.name,
                    phone: company.phone,
                    email: company.email,
                    address: company.address
                });
            }
        }
        return null;
    }

    public async getOrderForParticularCompany(companyName: string): Promise<Order[]> {
        const orders = await OrderModel.find({ companyName });
        // map to business model
        return orders.map((order) => {
            return new Order({
                orderId: order._id.toString(),
                orderNumber: order.orderNumber,
                company: {
                    companyId: order.company.companyId,
                    name: order.company.name,
                    // data not provided
                    email: '',
                    address: '',
                    phone: ''
                },
                customer: {
                    customerId: order.customer.customerId,
                    address: order.customer.address
                },
                product: {
                    productId: order.product.productId,
                    name: order.product.name,
                    currency: order.product.currency,
                    price: order.product.price
                }
            });
        });
    }
    private async findById(id: string): Promise<ICompanyModel> {
        return CompanyModel.findById(id);
    }
}
