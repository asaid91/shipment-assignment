import { IOrderModel } from './../models/OrderModel';
import { Order } from './../../../../models/Order';
import { BaseOrderRepository } from '../../../base-repositories/BaseOrderRepository';
import { OrderModel } from '../models/OrderModel';

export class OrderRepository implements BaseOrderRepository {

    /**
     * find order by "order number"
     * @param orderNumber
     */
    public async findByOrderNumber(orderNumber: string): Promise<Order> {
        const order = await OrderModel.findOne({ orderNumber });
        if (order !== null) {
            return this.findOne(order._id);
        }
        return null;
    }

    /**
     * add new order
     * @param model model containing all information about the order
     */
    public async createOrder(model: Order): Promise<Order> {

        const order = new OrderModel({
            orderNumber: model.orderNumber,
            product: {
                productId: model.product.productId,
                name: model.product.name,
                price: model.product.price,
                currency: model.product.currency
            },
            customer: {
                customerId: model.customer.customerId,
                address: model.customer.address
            },
            // we will keep this property as it is, because we don't want to break retro compatibility
            companyName: model.company.name,
            // instead we will create another object containing all other company related data
            // TODO migrating the old data in the next migration plan
            // so we will accept redundancy over breaking compatibility
            company: {
                companyId: model.company.companyId,
                name: model.company.name
            }
        });

        await order.save();
        return this.findOne(order._id);
    }

    /**
     * Find single order
     * @param id order identity
     */
    public async findOne(id: string): Promise<Order> {
        const order = await OrderModel.findById(id);
        if (order !== null) {
            return new Order({
                orderId: order._id.toString(),
                orderNumber: order.orderNumber,
                company: {
                    companyId: order.company.companyId,
                    name: order.company.name,
                    email: '',
                    address: '',
                    phone: ''
                },
                customer: {
                    customerId: order.customer.customerId,
                    address: order.customer.address
                },
                product: {
                    productId: order.product.productId,
                    name: order.product.name,
                    currency: order.product.currency,
                    price: order.product.price
                }
            });
        }
        return null;

    }
    /**
     * Find all orders
     */
    public async findAll(): Promise<Order[]> {
        // get all orders
        const orders = await OrderModel.find();

        // map to business model
        return orders.map((order) => {
            return new Order({
                orderId: order._id.toString(),
                orderNumber: order.orderNumber,
                company: {
                    companyId: order.company.companyId,
                    name: order.company.name,
                    // data not provided
                    email: '',
                    address: '',
                    phone: ''
                },
                customer: {
                    customerId: order.customer.customerId,
                    address: order.customer.address
                },
                product: {
                    productId: order.product.productId,
                    name: order.product.name,
                    currency: order.product.currency,
                    price: order.product.price
                }
            });
        });
    }

    /**
     * Delete a particular order given an OrderId
     * @param id OrderId to be deleted
     */
    public async delete(id: string): Promise<boolean> {
        await OrderModel.findOneAndRemove(id);
        return true;
    }

    public async getOrderToParticularAddress(address: string): Promise<Order[]> {
        const orders = await OrderModel.find({ 'customer.address': address });
        // map to business model
        return orders.map((order) => {
            return new Order({
                orderId: order._id.toString(),
                orderNumber: order.orderNumber,
                company: {
                    companyId: order.company.companyId,
                    name: order.company.name,
                    // data not provided
                    email: '',
                    address: '',
                    phone: ''
                },
                customer: {
                    customerId: order.customer.customerId,
                    address: order.customer.address
                },
                product: {
                    productId: order.product.productId,
                    name: order.product.name,
                    currency: order.product.currency,
                    price: order.product.price
                }
            });
        });
    }
}
