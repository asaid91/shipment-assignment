import { OrderModel } from './../models/OrderModel';
import { BaseProductRepository } from '../../../base-repositories/BaseProductRepository';
import { ProductModel } from '../models/ProductModel';
import { Product } from '../../../../models/Product';
export class ProductRepository implements BaseProductRepository {

    /**
     * Display how often each item has been ordered, in descending order
     */
    public async getStatistics(): Promise<any> {
        const data = await OrderModel.aggregate([
            {
                $group:
                    {
                        _id: {
                            productId: '$product.productId',
                            name: '$product.name'
                        },
                        total: { $sum: 1 }
                    }

            },
            {
                $project: {
                    productId: '$_id.productId',
                    productName: '$_id.name',
                    total: 1,
                    _id: 0
                }
            },
            { $sort: { total: -1 }}
        ]);
        return data;
    }
    public async findProduct(id: string): Promise<Product> {
        const product = await ProductModel.findById(id);
        if (product !== null) {
            return new Product({
                productId: product._id.toString(),
                name: product.name,
                price: product.price,
                currency: product.currency
            });
        }
        return null;
    }

}
