import * as Mongoose from 'mongoose';
import { Schema } from 'mongoose';

// order info
export interface IOrder {
    orderNumber: string;
    customer: {
        customerId: string,
        address: string
    };
    product: {
        productId: string,
        name: string,
        price: number,
        currency: string
    };

    company: {
        companyId: string
        name: string
    };
    companyName: string;

}
export interface IOrderModel extends IOrder, Mongoose.Document { }

// define db schema
const orderSchema = new Mongoose.Schema({
    orderNumber: String,
    customer: {
        customerId: String,
        address: String
    },
    companyName: String,
    product: {
        productId: String,
        name: String,
        price: Number,
        currency: String
    },

    company: {
        companyId: String,
        name: String
    }
});
export const OrderModel: Mongoose.Model<IOrderModel> = Mongoose.model<IOrderModel>('Order', orderSchema);
