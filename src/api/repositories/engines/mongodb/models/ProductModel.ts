import * as Mongoose from 'mongoose';

interface IProduct {
    name: string;
    price: number;
    currency: string;
}
interface IProductModel extends IProduct, Mongoose.Document { }

// define db schema
const productSchema = new Mongoose.Schema({
    name: String,
    price: Number,
    currency: String
});

export const ProductModel: Mongoose.Model<IProductModel> = Mongoose.model<IProductModel>('Product', productSchema);


