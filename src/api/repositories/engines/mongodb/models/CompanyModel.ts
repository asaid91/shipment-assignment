import { OrderModel } from './OrderModel';
import * as Mongoose from 'mongoose';

interface ICompany {
    name: string;
    phone: string;
    email: string;
    address: string;
}
export interface ICompanyModel extends ICompany, Mongoose.Document { }

// define db schema
const companySchema = new Mongoose.Schema({
    name: String,
    phone: String,
    email: String,
    address: String
});


export const CompanyModel: Mongoose.Model<ICompanyModel> = Mongoose.model<ICompanyModel>('Company', companySchema);


