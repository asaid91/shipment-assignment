import * as Mongoose from 'mongoose';

interface ICustomer {
    address: string;
}
interface ICustomerModel extends ICustomer, Mongoose.Document { }

// define db schema
const customerSchema = new Mongoose.Schema({
    address: String,
});

export const CustomerModel: Mongoose.Model<ICustomerModel> = Mongoose.model<ICustomerModel>('Customer', customerSchema);


