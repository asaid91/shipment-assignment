import { Product } from './../../models/Product';
export interface BaseProductRepository {
    findProduct(id: string): Promise<Product>;
    getStatistics(): Promise<any>;
}
