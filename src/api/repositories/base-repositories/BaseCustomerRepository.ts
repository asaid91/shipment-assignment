import { Customer } from './../../models/Customer';
export interface BaseCustomerRepository {
    findCustomer(customerId: string): Promise<Customer>;
}
