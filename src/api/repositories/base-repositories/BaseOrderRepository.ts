import { Order } from './../../models/Order';
export interface BaseOrderRepository {
    createOrder(model: Order): Promise<Order>;
    findAll(): Promise<Order[]>;
    findOne(id: string): Promise<Order>;
    findByOrderNumber(orderNumber: string): Promise<Order>;
    delete(id: string): Promise<boolean>;
    getOrderToParticularAddress(address: string): Promise<Order[]>;

}
