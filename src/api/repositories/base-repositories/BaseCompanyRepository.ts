import { Order } from './../../models/Order';
import { Company } from './../../models/Company';
export interface BaseCompanyRepository {
    getCompaniesForCertainItem(productId: string): Promise<Company[]>;
    editCompany(model: Company): Promise<boolean>;
    findCompany(id: string): Promise<Company>;
    getOrderForParticularCompany(companyName: string): Promise<Order[]>;
    getTotalAmount(id: string): Promise<number>;
}
