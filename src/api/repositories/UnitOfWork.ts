import { BaseCustomerRepository } from './base-repositories/BaseCustomerRepository';
import { CustomerRepository } from './engines/mongodb/repos/CustomerRepository';
import { CompanyRepository } from './engines/mongodb/repos/CompanyRepository';
import { OrderRepository } from './engines/mongodb/repos/OrderRepository';
import { ProductRepository } from './engines/mongodb/repos/ProductRepository';
import { BaseOrderRepository } from '../repositories/base-repositories/BaseOrderRepository';
import { BaseCompanyRepository } from './base-repositories/BaseCompanyRepository';
import { BaseProductRepository } from './base-repositories/BaseProductRepository';

export enum Engines {
    Mongodb,
    MySQL
}
export class UnitOfWork {
    // set MongoDB as default database
    public static engine = Engines.Mongodb;

    // instantiate Order Repository
    static get OrderRepository(): BaseOrderRepository {
        if (UnitOfWork.engine === Engines.Mongodb) {
            return new OrderRepository();
        }
        throw new Error('No repository injected');
    }

    // instantiate Company Repository
    static get CompanyRepository(): BaseCompanyRepository {
        if (UnitOfWork.engine === Engines.Mongodb) {
            return new CompanyRepository();
        }
        throw new Error('No repository injected');
    }


    // instantiate Product Repository
    static get ProductRepository(): BaseProductRepository {
        if (UnitOfWork.engine === Engines.Mongodb) {
            return new ProductRepository();
        }
        throw new Error('No repository injected');
    }

        // instantiate Customer Repository
        static get CustomerRepository(): BaseCustomerRepository {
            if (UnitOfWork.engine === Engines.Mongodb) {
                return new CustomerRepository();
            }
            throw new Error('No repository injected');
        }
}
