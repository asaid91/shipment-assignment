import { UnitOfWork } from './../repositories/UnitOfWork';
import { OrderRepository } from './../repositories/engines/mongodb/repos/OrderRepository';
import { CompanyModel } from './../repositories/engines/mongodb/models/CompanyModel';
import { CustomerModel } from './../repositories/engines/mongodb/models/CustomerModel';
import { Order } from './../models/Order';
import { CustomerNotFound } from './../errors/CustomerNotFound';
import { OrderAlreadyExist } from './../errors/OrderAlreadyExist';
import { CreateOrder } from '../controllers/requests/CreateOrder';
import { Service } from 'typedi';
import { ProductNotFound } from '../errors/ProductNotFound';
import { CompanyNotFound } from '../errors/CompanyNotFound';
import { Validator } from 'class-validator';

const OrderModel: any = null;
@Service()
export class OrderService {

    /**
     * Find all orders
     */
    public async findAll(): Promise<Order[]> {
        return UnitOfWork.OrderRepository.findAll();
    }


    /**
     * Find order by Id
     * @param Id order identity
     */
    public async findOrder(id: string): Promise<Order> {
        return UnitOfWork.OrderRepository.findOne(id);
    }

    /**
     *  Add new order
     * @param model Order model related data to be inserted
     */
    public async addNewOrder(model: CreateOrder): Promise<Order> {
        // check order exists
        if ((await UnitOfWork.OrderRepository.findByOrderNumber(model.orderNumber)) !== null) {
            throw new OrderAlreadyExist();
        }

        // get product details
        const product = await UnitOfWork.ProductRepository.findProduct(model.productId);
        if (product === null) {
            throw new ProductNotFound();
        }

        // get customer details
        const customer = await UnitOfWork.CustomerRepository.findCustomer(model.customerId);
        if (customer === null) {
            throw new CustomerNotFound();
        }
        // get company details
        const company = await CompanyModel.findById(model.companyId);
        if (company === null) {
            throw new CompanyNotFound();
        }

        const order = new Order({
            orderNumber: model.orderNumber,
            company: {
                companyId: company.id,
                name: company.name
            },
            customer: {
                customerId: customer.customerId,
                address: customer.address
            },
            product: {
                productId: product.productId,
                name: product.name,
                currency: product.currency,
                price: product.price
            }
        });

        return UnitOfWork.OrderRepository.createOrder(order);
    }

    /**
     *  Show all orders from a particular company
     *  Note: we changed this method to work in both property & object in order to keep the old
     *  dependency until we migrate the data in depend fully on the new schema.
     * @param companyId CompanyId (optional)
     * @param companyName Company Name (optional)
     */
    public async getOrderForParticularCompany(companyId?: string, companyName?: string): Promise<any> {
        // get company name in case companyId passed
        if (companyId && new Validator().isMongoId(companyId)) {
            const company = await UnitOfWork.CompanyRepository.findCompany(companyId);
            if (company !== null) {
                companyName = company.name;
            }
        }
        return UnitOfWork.CompanyRepository.getOrderForParticularCompany(companyName);
    }

    /**
     * Show all orders to a particular address
     * @param address Address
     */
    public async getOrderToParticularAddress(address: string): Promise<Order[]> {
        const orders = UnitOfWork.OrderRepository.getOrderToParticularAddress(address);
        return orders;
    }

    /**
     * Delete a particular order given an OrderId
     * @param orderId OrderId to be deleted
     */
    public async deleteOrder(orderId: string): Promise<boolean> {
        return await UnitOfWork.OrderRepository.delete(orderId);
    }
}
