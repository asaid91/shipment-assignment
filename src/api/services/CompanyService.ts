import { CompanyRepository } from './../repositories/engines/mongodb/repos/CompanyRepository';
import { CompanyModel } from './../repositories/engines/mongodb/models/CompanyModel';
import { Company } from './../models/Company';
import { UnitOfWork } from './../repositories/UnitOfWork';
import { EditCompany } from './../controllers/requests/EditCompany';
import { Service } from 'typedi';
import { Validator, IsMongoId } from 'class-validator';
import { CompanyNotFound } from '../errors/CompanyNotFound';
import { OrderModel } from '../repositories/engines/mongodb/models/OrderModel';


@Service()
export class CompanyService {


    /**
     * Edit existing company
     * @param model Company model related data to be edited
     */
    public async editCompany(id: string, model: EditCompany): Promise<boolean> {
        const company = await UnitOfWork.CompanyRepository.findCompany(id);
        if (company !== null) {
            const companyModel = new Company({
                companyId: id,
                name: model.companyName,
                address: model.address,
                email: model.email,
                phone: model.phone
            });
            // edit the company record
            return await UnitOfWork.CompanyRepository.editCompany(companyModel);
        }
        throw new CompanyNotFound();
    }

    /**
     * Find company by ID
     * @param id Company ID
     */
    public async findCompany(id: string): Promise<Company> {
        return UnitOfWork.CompanyRepository.findCompany(id);
    }

    /**
     * Get the amount of money paid by a company
     * @param company company info
     */
    public async getTotalAmount(companyId: string): Promise<number> {
        const total = UnitOfWork.CompanyRepository.getTotalAmount(companyId);
        return total;
    }

    /**
     * Get companies which (sold) a certain item
     * @param itemId item id
     */
    public async getCompaniesForCertainItem(productId: string): Promise<Company[]> {
        return UnitOfWork.CompanyRepository.getCompaniesForCertainItem(productId);
    }
}
