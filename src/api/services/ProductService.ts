import { UnitOfWork } from './../repositories/UnitOfWork';
import { Service } from 'typedi';
import { Validator } from 'class-validator';
import { Product } from '../models/Product';

@Service()
export class ProductService {

    /**
     * Find product by ID
     * @param id
     */
    public async findProduct(id: string): Promise<Product> {
        return await UnitOfWork.ProductRepository.findProduct(id);
    }

    public async getStatistics(): Promise<any> {
        return await UnitOfWork.ProductRepository.getStatistics();
    }
}
