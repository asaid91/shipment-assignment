import { HttpError } from 'routing-controllers';

export class ProductNotFound extends HttpError {
    constructor() {
        super(400, 'Product not found!');
    }
}
