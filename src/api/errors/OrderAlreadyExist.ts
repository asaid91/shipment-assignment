import { HttpError } from 'routing-controllers';

export class OrderAlreadyExist extends HttpError {
    constructor() {
        super(400, 'Order already exists!');
    }
}
