import { HttpError } from 'routing-controllers';

export class CustomerNotFound extends HttpError {
    constructor() {
        super(400, 'Customer not found!');
    }
}
