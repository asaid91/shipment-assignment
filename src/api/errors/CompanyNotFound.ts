import { HttpError } from 'routing-controllers';

export class CompanyNotFound extends HttpError {
    constructor() {
        super(400, 'Company not found!');
    }
}
