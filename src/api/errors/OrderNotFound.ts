import { HttpError } from 'routing-controllers';

export class OrderNotFound extends HttpError {
    constructor() {
        super(400, 'Order not found!');
    }
}
