export class Order {
    public orderId: any;
    public orderNumber: string;
    public customer: {
        customerId: any,
        address: string,
    };

    public product: {
        productId: any,
        name: string,
        price: number,
        currency: string
    };

    public company: {
        companyId: any,
        name: string,
        phone?: string,
        email?: string,
        address?: string
    };
    public constructor(init?: Partial<Order>) {
        Object.assign(this, init);
    }
}
