export class Company {
    public companyId: any;
    public name: string;
    public phone: string;
    public email: string;
    public address: string;

    public constructor(init?: Partial<Company>) {
        Object.assign(this, init);
    }
}
