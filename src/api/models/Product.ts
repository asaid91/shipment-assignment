export class Product {
    public productId: string;
    public name: string;
    public price: number;
    public currency: string;

    public constructor(init?: Partial<Product>) {
        Object.assign(this, init);
    }
}
