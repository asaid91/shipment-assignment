export class Customer {
    public customerId: string;
    public address: string;

    public constructor(init?: Partial<Customer>) {
        Object.assign(this, init);
    }
}
