import { DynamicResponse } from './responses/DynamicResponse';
import { BaseResponse } from './responses/BaseResponse';
import { ProductService } from '../services/ProductService';
import { ProductNotFound } from './../errors/ProductNotFound';
import { JsonController, Get, Param } from 'routing-controllers';
import { CompanyService } from '../services/CompanyService';
import { CompanyListHttpResponse, CompanyResponse } from './responses/CompanyResponse';



@JsonController('/products')
export class ProductController {

    // inject services into private constructor using Dependency Injection
    constructor(private companyService: CompanyService,
                private productService: ProductService) { }


    /**
     * Get all companies that bought a certain orderItem
     * @param productId
     */
    @Get('/:product_id/companies')
    public async getCompaniesForCertainItem( @Param('product_id') productId: string): Promise<CompanyListHttpResponse> {

        const product = await this.productService.findProduct(productId);

        // product exist
        if (product !== null) {
            const companies = await this.companyService.getCompaniesForCertainItem(productId);
            return {
                success: true,
                message: 'success',
                data: companies.map((company) => {
                    return {
                        id: company.companyId,
                        name: company.name,
                        email: company.email,
                        phone: company.phone,
                        address: company.address,
                    } as CompanyResponse;
                })
            } as CompanyListHttpResponse;
        }

        throw new ProductNotFound();
    }

    /**
     * Display how often each item has been ordered, in descending order
     * @param productId
     */
    @Get('/statistics')
    public async getProductsStatistics(): Promise<CompanyListHttpResponse> {

        const statistics = await this.productService.getStatistics();
        return {
            success: true,
            message: 'success',
            data: statistics
        } as DynamicResponse<any>;
    }

}
