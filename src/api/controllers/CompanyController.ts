import { DynamicResponse } from './responses/DynamicResponse';
import { OrderListHttpResponse, OrderResponse } from './responses/OrderResponse';
import { CompanyHttpResponse } from './responses/CompanyResponse';
import { CompanyService } from './../services/CompanyService';
import { CompanyNotFound } from '../errors/CompanyNotFound';
import { OrderService } from './../services/OrderService';
import { JsonController, Get, Param, Put, Body } from 'routing-controllers';
import { EditCompany } from '../controllers/requests/EditCompany';
import { Validator } from 'class-validator';



@JsonController('/company')
export class CompanyController {

    // inject services into private constructor using Dependency Injection
    constructor(private orderService: OrderService,
                private companyService: CompanyService) { }

    @Get('/:companyName/orders')
    public async findRelatedOrders( @Param('companyName') companyName: string): Promise<OrderListHttpResponse> {
        const data = await this.orderService.getOrderForParticularCompany(undefined, companyName);
        return {
            success: true,
            message: 'success',
            data: data.map((order) => {
                return new OrderResponse({
                    id: order.orderId,
                    orderNumber: order.orderNumber,
                    companyName: order.company.name,
                    customerAddress: order.customer.address,
                    productName: order.product.name,
                    price: order.product.price,
                    currency: order.product.currency
                });
            })
        } as OrderListHttpResponse;
    }

    /**
     * A new version of the same api, in order to not breaking compatibility
     * The new one takes companyId instead of companyName, because we now have a specific company entity
     * despite the first one was only depending on company address
     * @param companyId
     */
    @Get('/v2/:id/orders')
    public async findRelatedOrdersV2( @Param('id') companyId: string): Promise<OrderListHttpResponse> {
        const data = await this.orderService.getOrderForParticularCompany(companyId, undefined);
        return {
            success: true,
            message: 'success',
            data: data.map((order) => {
                return new OrderResponse({
                    id: order.orderId,
                    orderNumber: order.orderNumber,
                    companyName: order.company.name,
                    customerAddress: order.customer.address,
                    productName: order.product.name,
                    price: order.product.price,
                    currency: order.product.currency
                });
            })
        } as OrderListHttpResponse;
    }

    @Get('/:id')
    public async one( @Param('id') id: string): Promise<CompanyHttpResponse> {
        const company = await this.companyService.findCompany(id);
        if (company !== null) {
            return {
                success: true,
                message: 'success',
                data: {
                    id: company.companyId,
                    address: company.address,
                    phone: company.phone,
                    email: company.email,
                    name: company.name
                }
            } as CompanyHttpResponse;
        }
        throw new CompanyNotFound();
    }

    @Put('/:id')
    public async edit( @Param('id') id: string, @Body() model: EditCompany): Promise<CompanyHttpResponse> {
        await this.companyService.editCompany(id, model);
        const company = await this.companyService.findCompany(id);
        return {
            success: true,
            message: 'success',
            data: {
                id: company.companyId,
                address: company.address,
                phone: company.phone,
                email: company.email,
                name: company.name
            }
        } as CompanyHttpResponse;
    }

    // Get the amount of money paid by a certain company
    @Get('/:id/total')
    public async getTotalAmount( @Param('id') id: string): Promise<DynamicResponse<number>> {
        const company = await this.companyService.findCompany(id);
        if (company !== null) {
            const total = await this.companyService.getTotalAmount(id);
            return {
                success: true,
                message: 'success',
                data: total
            } as DynamicResponse<number>;
        }
        throw new CompanyNotFound();
    }
}
