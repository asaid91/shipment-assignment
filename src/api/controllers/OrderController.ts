import { BaseResponse } from './responses/BaseResponse';
import { OrderListHttpResponse, OrderResponse, OrderHttpResponse } from './responses/OrderResponse';
import { Order } from './../models/Order';
import { OrderNotFound } from './../errors/OrderNotFound';
import { CreateOrder } from './requests/CreateOrder';
import { OrderService } from './../services/OrderService';
import { JsonController, Get, Post, Param, Delete, Body, QueryParam, HttpCode } from 'routing-controllers';



@JsonController('/orders')
export class OrderController {

    constructor(private orderService: OrderService) { }

    @Get()
    public async find( @QueryParam('address') address: string): Promise<OrderListHttpResponse> {
        let data: Order[] = [];
        if (address) {
            data = await this.orderService.getOrderToParticularAddress(address);
        } else {
            data = await this.orderService.findAll();
        }

        return {
            success: true,
            message: 'success',
            data: data.map((order) => {
                return new OrderResponse({
                    id: order.orderId,
                    orderNumber: order.orderNumber,
                    companyName: order.company.name,
                    customerAddress: order.customer.address,
                    productName: order.product.name,
                    price: order.product.price,
                    currency: order.product.currency
                });
            })
        } as OrderListHttpResponse;
    }

    @Post()
    @HttpCode(201)
    public async create( @Body() order: CreateOrder): Promise<OrderHttpResponse> {
        const orderCreated = await this.orderService.addNewOrder(order);
        return {
            success: true,
            message: 'success',
            data: {
                id: orderCreated.orderId,
                orderNumber: orderCreated.orderNumber,
                companyName: orderCreated.company.name,
                customerAddress: orderCreated.customer.address,
                productName: orderCreated.product.name,
                price: orderCreated.product.price,
                currency: orderCreated.product.currency
            }
        } as OrderHttpResponse;
    }


    @Get('/:id')
    public async one( @Param('id') id: string): Promise<OrderHttpResponse> {
        const order = await this.orderService.findOrder(id);
        if (order !== null) {
            return {
                success: true,
                message: 'success',
                data: {
                    id: order.orderId,
                    orderNumber: order.orderNumber,
                    companyName: order.company.name,
                    customerAddress: order.customer.address,
                    productName: order.product.name,
                    price: order.product.price,
                    currency: order.product.currency
                }
            } as OrderHttpResponse;
        }
        throw new OrderNotFound();
    }


    @Delete('/:id')
    public async delete( @Param('id') id: string): Promise<BaseResponse> {
        const order = await this.orderService.findOrder(id);
        if (order !== null) {
            await this.orderService.deleteOrder(id);
            return {
                success: true,
                message: 'deleted',
            } as BaseResponse;
        }
        throw new OrderNotFound();
    }

}
