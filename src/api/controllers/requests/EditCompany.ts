import { IsNotEmpty, IsMobilePhone, IsEmail } from 'class-validator';

export class EditCompany {

    @IsNotEmpty()
    public companyName: string;

    // @IsMobilePhone('en-US')
    @IsNotEmpty()
    public phone: string;

    @IsEmail()
    @IsNotEmpty()
    public email: string;

    public address: string;
}
