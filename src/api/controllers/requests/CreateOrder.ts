import { IsNotEmpty, IsMongoId } from 'class-validator';

export class CreateOrder {

    @IsNotEmpty()
    public orderNumber: string;

    @IsNotEmpty()
    @IsMongoId()
    public customerId: string;

    @IsNotEmpty()
    @IsMongoId()
    public companyId: string;

    @IsNotEmpty()
    @IsMongoId()
    public productId: string;
}
