import { BaseResponse } from './BaseResponse';
export interface CustomerResponse {
    id: string;
    address: string;
}

export interface CustomerHttpResponse extends BaseResponse {
    data: CustomerResponse;
}


export interface CustomerListHttpResponse extends BaseResponse {
    data: CustomerResponse[];
}
