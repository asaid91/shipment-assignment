import { BaseResponse } from './BaseResponse';
export interface DynamicResponse<T> extends BaseResponse {
    data: T;
}
