import { BaseResponse } from './BaseResponse';
export interface CompanyResponse {
    id: string;

    name: string;

    phone: string;

    email: string;

    address: string;
}

export interface CompanyHttpResponse extends BaseResponse {
    data: CompanyResponse;
}

export interface CompanyListHttpResponse extends BaseResponse {
    data: CompanyResponse[];
}
