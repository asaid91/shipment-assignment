import { BaseResponse } from './BaseResponse';

export class OrderResponse {
    public id: string;
    public orderNumber: string;
    public companyName: string;
    public customerAddress: string;
    public productName: string;
    public price: number;
    public currency: string;

    public constructor(init?: Partial<OrderResponse>) {
        Object.assign(this, init);
    }
}

export interface OrderHttpResponse extends BaseResponse {
    data: OrderResponse;
}


export interface OrderListHttpResponse extends BaseResponse {
    data: OrderResponse[];
}
