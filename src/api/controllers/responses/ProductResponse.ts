import { BaseResponse } from './BaseResponse';
export interface ProductResponse {
    id: string;
    name: string;
    price: number;
    currency: string;
}

export interface ProductHttpResponse extends BaseResponse {
    data: ProductResponse;
}


export interface ProductListHttpResponse extends BaseResponse {
    data: ProductResponse[];
}
