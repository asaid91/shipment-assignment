# BorderGurgu Technical Assignment

> Please find attached information related to project and questions answers.

## New Enhancements, Please pull it!
- Unit Testing has been added.
- End to end (e2e) test has been added to test endpoints.
- Separation of concern for database, services layer, and controller layer.
- "Unit of Work" and "Repository" design pattern is implemented for
encapsulating the database layer.
Bugs solved.



## Table of Contents

- [Questions](#questions)
- [Getting Started](#getting-Started)
- [API Routes](#api-routes)
- [Project Structure](#project-structure)

## Questions
### Why did you pick your first particular your design? What assumptions did you make?

## Business
While building an application the questions have to take place in order to help extrapolates all available information we can get.
As the current provided data is not suitable for business production level, so..

examples of questions should take place:
 
 - Do we ship multiple product in the same order?
 - How should we deal with the `Currency Changes` between `Companies` & `Clients`.
 - Information about shipping costs.
 - Company info details.
 - Customer info details.

 
## Technical

>I chose to make a scalable application structure using Typescript/NodeJS, routing-controller, and other cool stuff, in production level i will consider doing more things like:

- Go with more high level infrastructure like `SOA, Micro-Services, Domain Driven Design`.
- Making `Separation of Concerns` on every part in the application to enhance `Modularity & Scalability`. 
- Implementing `Repository` & `Unit of Work` design patterns.
- Take more time thinking about the appropriate data modeling specially while working with `NO-SQL` databases, considering `Performance & Redundancy` Issues, [...etc].

### What did you consider when you had to design the second solution?And which assumptions and tradeoffs you made??

### Considerations
 - Not breaking retro compatibility.
 - Existing APIs which depend on the old schema.
 - Analyzing the dependencies well, in order to take the proper decision.
 - Add versioning to effected endpoints.
 - Is performance matters, in this case?
 - How much data changes could take place?
 
### Tradeoffs

- Add new separate collection and reference it as new property.
    - Pros: Will reduce redundancy.
    - Cons: Very costly queries.
- Embed the new company entity object into the order document.
    - Pros:  
        - Keep history version of document permanent.
        - Very speed reading quires, performance is good.
    - Cons: Base entities updates and handling reflections on related data, is very painful. 


### What do you like (and dislike) about Node/Javascript compared to other programming languages?

## Things i like
- A Good choice for huge traffic web sites.
- Asynchronous event driven architecture helps concurrent request handling.
- Non-Blocking Code.
- V8 JavaScript Runtime engine is very speed.
- Write Javascript/Typescript everywhere in both client side and server side.
- Community is going crazy contributions with it, very promising.

## Things i don't like
- Javascript was for large scale application was messy, now with Typescript is heaven.
- No Multi-Threading.
- Single Thread is hell with time consuming synchronous operations.




## Getting Started

### Step 1:  Set up the Development Environment

You need to set up your development environment before you can run the application.

Install [Node.js and NPM](https://nodejs.org/en/download/)


I used yarn as a "Packaging Manager", Install yarn globally

```bash
npm install yarn -g
```

Install a MongoDB database.

> I'm using MongoDB Atlas Cloud service, it's free for use without hassle.

```bash
mongodb://ahmedsaid:<PASSWORD>@borderguru-cluster1-shard-00-00-gopjg.mongodb.net:27017,borderguru-cluster1-shard-00-01-gopjg.mongodb.net:27017,borderguru-cluster1-shard-00-02-gopjg.mongodb.net:27017/test?ssl=true&replicaSet=BorderGuru-Cluster1-shard-0&authSource=admin
```
replace password with : `nJX8HPsPB0I4beWJ`

### Step 2: Setup Environment

Copy the `.env.example` file and rename it to `.env`. In this file we can modify any configuration data like:
- Application Basic Configuration
- Logging Configuration
- Database Configuration
- Swagger Configuration



Setup application environment.

```bash
npm run setup
```

> This installs all dependencies with yarn.

### Step 3: Serve the application

We can serve the application using this command, this should listen to file changes by `nodemon` in re-run the application with `ts-node`

```bash
npm start serve
```

> No you can browser the application via `http://0.0.0.0:3000`

## Scripts and Tasks

All script are defined in the package.json

### Install

- Install all dependencies with `yarn install`

### Linting

- Run code `npm start lint`. This runs tslint.

### Tests

- Run the unit tests using `npm start test`.
- Run the e2e tests using `npm start test:e2e`.

### Test Results

![Unit Testing Execution Result](https://preview.ibb.co/etHnJ7/Screen_Shot_2018_02_18_at_4_35_30_PM.png)

![E2E Execution Result](https://preview.ibb.co/mNMEy7/Screen_Shot_2018_02_18_at_3_55_16_PM.png)

>(slow down warning because of i'm using a cloud db)

### Running in dev mode

- Run `npm start serve` to start nodemon with ts-node, to serve the app.
- The server address will be displayed to you as `http://0.0.0.0:3000`

### Building the project and run it

- Run `npm start build` to generated all JavaScript files from the TypeScript sources.
- To start the builded app located in `dist` use `npm start`.


## API Routes

The swagger and the monitor route can be altered in the `.env` file.
#### Note: We use `/api` as prefix by default

| Route          | Method | Description |
| -------------- | ----------- | ----------- |
| **/swagger**   | GET | This is the Swagger UI with our API  documentation |
| **/api/orders** | GET | Retrieve all orders exists in the system |
| **/api/orders** | POST | Create new order |
| **/api/orders/{id}** | GET | Returns the given order information |
| **/api/orders/{id}** | DELETE | Delete Order |
| **/api/company/{id}** | GET | Returns the given company information |
| **/api/company/{id}/total** | GET | Get the amount of money paid by a company |
| **/api/company/{id}** | PUT | Update Company information |
| **/api/company/{companyName}/orders** | GET | Show all orders from a particular company |
| **/api/company/v2/{id}/orders** | GET | Show all orders from a particular company (Version 2), After the new changes take place! |
| **/api/company/{id}/total** | GET | Get the amount of money paid by a company |
| **/api/products/{product_id}/companies** | GET | Get all companies that bought a certain orderItem |


## Project Structure

| Name                              | Description |
| --------------------------------- | ----------- |
| **.vscode/**                      | VSCode tasks, launch configuration and some other settings |
| **dist/**                         | Compiled source files will be placed here |
| **src/**                          | Source files |
| **src/api/controllers/**          | REST API Controllers |
| **src/api/controllers/requests**  | Request classes with validation rules if the body is not equal with a model |
| **src/api/controllers/responses** | Response classes or interfaces to type json response bodies  |
| **src/api/errors/**               | Custom HttpErrors like 404 NotFound |
| **src/api/middlewares/**          | Express Middlewares |
| **src/api/models/**               | Models |
| **src/api/services/**             | Service layer |
| **src/api/** swagger.json         | Swagger documentation |
| **src/core/**                     | The core features like logger and env variables |
| **src/loaders/**                  | Loader is a place where you can configure your app |
| **src/types/** *.d.ts             | Custom type definitions and files that aren't on DefinitelyTyped |
| **test**                          | Tests |
| **test/e2e/** *.test.ts           | End-2-End tests (2e2) |
| **test/unit/** *.test.ts          | Unit tests |
| .env.example                      | Environment configurations |
| .env.test                         | Test environment configurations |

